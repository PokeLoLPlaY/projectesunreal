# Runner

Risquetos Bàsics:

- [X] Controls simples, ja siui clicar o un o dos botons
- [X] Moviment per fìsiques i amb collisions
- [X] Creació d'objectes amb un spawner (Relacionat amb Risketo Opcional)
- [X] Aleatorietat a qualsevol lloc del joc
- [X] Puntuació mostrada per pantalla, no val debug

Risquetos Opcionals:

- [X] Death
- [X] Augment de dificultat
- [X] Parallax

Controles:
- Espacio saltar
- A y D per a moure's horitzontalment

# TowerDefense

Risquetos Bàsics:

- [X] Capacitat d’elecció, per exemple diferents torretes.
- [X] El HUD es crea i s’emplena per BP.
- [X] Ús d’esdeveniments i dispatchers, tot ha d’estar desacoplat.
- [X] Enemics o objectius a on atacar i en fer-ho que morin.
- [X] Ús d’un actor component o Interfície.

Risquetos Opcionals:

- [X] “Waves” personalitzables a on posar quantitat d’enemics i tipus d’enemics a aparèixer.
- [X] Prebuilding visual
- [X] Animació HUD
- [X] Moviment de càmara

Controles:
- Rightclick open Menu
- LeftClick select
- W A S D movement

# Shooter

Risquetos Bàsics:

- [X] Control de personatge amb base ACharacter (o amb AController).
- [X] Capacitat de disparar, ja sigui amb Trace line (Raycast) o amb objectes físics en moviment (Projectile).
- [X] Utilitzar delegats per fer alguna acció, per exemple el disparar.
- [X] Més d’un tipus d'armes i/o bales.
- [X] Ús de les UMacros per privatització, escritura i lectura de forma correcta.


Risquetos Opcionals:


Controles:
- Space Jump
- LeftClick shoot
- W A S D movement

# RPG

Risquetos Bàsics:

- [X] Un sistema preparat per tenir diverses classes, només se'n requereix un però ha d'estar preparat per ampliar, per exemple mag. En aquest ha de ser l'estructura de dades per a les estadístiques del jugador, vida, manà, energia, dany físic, dany magic, etc. Això és al gust de cada joc.
- [X] Un sistema de combat que et permeti atacar, llançar habilitats i rebre mal.
- [X] Clarament un enemic que també ho pugui fer.
- [X] Un sistema d'habilitats que permeti al jugador canviar quina habilitat fer servir, ha de tenir com a mínim 2.


Risquetos Opcionals:
- [X] Enemigo que puede deanbular, atacar y percibir si el jugador esta en su rango de vision

Controles:
- 1 y 2 Elegir poderes (Solo ay BP del ataque 1)
- RightClick shoot
- LeftClick move



