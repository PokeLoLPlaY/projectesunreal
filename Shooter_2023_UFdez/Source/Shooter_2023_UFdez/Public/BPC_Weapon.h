
#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "BPC_Weapon.generated.h"

class APlayerCharacter;

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class SHOOTER_2023_UFDEZ_API UBPC_Weapon : public UActorComponent
{
	GENERATED_BODY()

public:	
	UBPC_Weapon();
	
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	UFUNCTION(BlueprintCallable)
	void AttachWeapoon(APlayerCharacter* apPlayerCharacter);

	UFUNCTION(BlueprintCallable)
	void OnFireCallback();

	UPROPERTY(EditDefaultsOnly)
	UBillboardComponent* mpMuzzelOffset;
	
	UPROPERTY(EditDefaultsOnly)
	float mShootRange{500.f};
	
	UPROPERTY(EditDefaultsOnly)
	float mDispersion;
	
	UPROPERTY(EditDefaultsOnly)
	int mNumBalas;

	UPROPERTY(EditDefaultsOnly,BlueprintReadOnly)
	float mDamage{2.f};
	
	UPROPERTY(EditDefaultsOnly,BlueprintReadOnly)
	APlayerCharacter* mpOwnerCharacter;

};