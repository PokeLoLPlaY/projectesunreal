
#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "EnemyCharacter.generated.h"

UCLASS()
class SHOOTER_2023_UFDEZ_API AEnemyCharacter : public ACharacter
{
	GENERATED_BODY()

public:
	AEnemyCharacter();
	
	virtual void Tick(float DeltaTime) override;

	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	UFUNCTION()
	void OnHitCallback(AActor* apDamagedActor, float apDamage, const UDamageType* apDamageType, AController* apInstigatedBy, AActor* apDamageCauser);
	
	UPROPERTY(EditDefaultsOnly)
		class UBoxComponent* mpBoxCollision;

	UPROPERTY(EditDefaultsOnly)
		float health;
	
protected:
	
	virtual void BeginPlay() override;


};
