// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "BPC_Weapon.h"
#include "InputActionValue.h"
#include "Camera/CameraComponent.h"
#include "GameFramework/Character.h"
#include "PlayerCharacter.generated.h"

class UInputMappingContext;
class UInputConfigData;

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnClick);

UCLASS()
class SHOOTER_2023_UFDEZ_API APlayerCharacter : public ACharacter
{
	GENERATED_BODY()

public:
	APlayerCharacter();

	virtual void Tick(float DeltaTime) override;

	void MoveCallBack(const FInputActionValue& aValue);
	
	void LookCallBack(const FInputActionValue& aValue);


	void OnClickCallback(const FInputActionValue& InputActionValue);
	
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;
	
	UStaticMeshComponent* GetWeapon() const{return mWeaponPoint;};
	UCameraComponent* GetCamera() const{return mpCamera;};

	//UCameraComponent* GetCamera() const { return mCamera; }
	
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
		UStaticMeshComponent* mWeaponPoint;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
		UInputMappingContext* mInputMapping;
	
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
		UInputConfigData* mInputData;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		int mMoveSpeed {2U};

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		float mCurrentHealth {100.f};

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		float mMaxHealth {100.f};

	UPROPERTY(BlueprintAssignable)
		FOnClick evOnClick;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	UBPC_Weapon* mpWeaponEquiped;

	

protected:
	virtual void BeginPlay() override;

private:
	UPROPERTY(VisibleAnywhere)
		UCameraComponent* mpCamera;

	

};
