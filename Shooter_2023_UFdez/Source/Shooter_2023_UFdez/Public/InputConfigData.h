#pragma once

#include "CoreMinimal.h"
#include "Engine/DataAsset.h"
#include "InputConfigData.generated.h"

/**
 * 
 */
class UInputAction;

UCLASS()
class SHOOTER_2023_UFDEZ_API UInputConfigData : public UDataAsset
{
	GENERATED_BODY()
public:
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
		UInputAction* InputMove;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
		UInputAction* InputLook;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
		UInputAction* InputJump;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
		UInputAction* InputMouseClick;
	
};
