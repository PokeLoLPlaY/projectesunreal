// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "Shooter_2023_UFdezGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class SHOOTER_2023_UFDEZ_API AShooter_2023_UFdezGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
