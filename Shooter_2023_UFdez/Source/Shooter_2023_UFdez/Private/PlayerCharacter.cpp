

#include "PlayerCharacter.h"
#include "EnhancedInputComponent.h"
#include "EnhancedInputSubsystems.h"
#include "InputConfigData.h"
#include "Components/CapsuleComponent.h"
#include "Utils.h"
#include "Shooter_2023_UFdez/Public/Utils.h"

APlayerCharacter::APlayerCharacter()
{
	PrimaryActorTick.bCanEverTick = true;

	mpCamera = CreateDefaultSubobject<UCameraComponent>(TEXT("MainCamera"));
	mpCamera->SetupAttachment(GetCapsuleComponent());
	mpCamera->bUsePawnControlRotation = true; 
	
	mWeaponPoint = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("WeaponPoint"));
	mWeaponPoint -> bCastDynamicShadow = false;
	mWeaponPoint -> CastShadow = false;

	//mInputMapping = CreateDefaultSubobject<UInputMappingContext>(TEXT("InputMapping"));
}

void APlayerCharacter::BeginPlay()
{
	Super::BeginPlay();
	mWeaponPoint->AttachToComponent(GetMesh(),{EAttachmentRule::SnapToTarget,true} ,TEXT("WeaponPointSocket"));
}


void APlayerCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void APlayerCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	check(PlayerInputComponent);

	APlayerController* PlayerController = Cast<APlayerController>(GetController());
	UEnhancedInputLocalPlayerSubsystem* EInputSubsystem = ULocalPlayer::GetSubsystem<UEnhancedInputLocalPlayerSubsystem>(PlayerController->GetLocalPlayer());
	EInputSubsystem->ClearAllMappings();
	EInputSubsystem->AddMappingContext(mInputMapping,0);

	UEnhancedInputComponent* EInputComponent {Cast<UEnhancedInputComponent>(PlayerInputComponent)};
	EInputComponent->BindAction(mInputData->InputMove, ETriggerEvent::Triggered, this, &APlayerCharacter::MoveCallBack);
	EInputComponent->BindAction(mInputData->InputLook, ETriggerEvent::Triggered, this, &APlayerCharacter::LookCallBack);
	EInputComponent->BindAction(mInputData->InputJump, ETriggerEvent::Triggered, this, &ACharacter::Jump);
	EInputComponent->BindAction(mInputData->InputMouseClick, ETriggerEvent::Started, this, &APlayerCharacter::OnClickCallback);

	mpWeaponEquiped=nullptr;
}

void APlayerCharacter::MoveCallBack(const FInputActionValue& aValue)
{
	if(IsValid(Controller))
	{
		const FVector2D MoveValue {aValue.Get<FVector2d>()};
		const FRotator MoveRotator {0,Controller->GetControlRotation().Yaw,0};

		if(MoveValue.Y !=0.0f)
		{
			const FVector DIR {MoveRotator.RotateVector(FVector::ForwardVector)};
			AddMovementInput(DIR,MoveValue.Y);
			
		}
		if(MoveValue.X !=0.0f)
		{
			const FVector DIR {MoveRotator.RotateVector(FVector::RightVector)};
			AddMovementInput(DIR,MoveValue.X);
			
		}
	}
}

void APlayerCharacter::LookCallBack(const FInputActionValue& aValue)
{
	if(IsValid(Controller))
	{
		const FVector2D LookValue {aValue.Get<FVector2D>()};

		if(LookValue.X !=0.0f)
		{
			AddControllerYawInput(LookValue.X);
		}
		if(LookValue.Y !=0.0f)
		{
			AddControllerPitchInput(LookValue.Y);
		}
	}
}

void APlayerCharacter::OnClickCallback(const FInputActionValue& InputActionValue)
{
	if(IsValid(Controller))
	{
		evOnClick.Broadcast();
		if(GEngine!=nullptr)
		{
			//ScreenD(FString::Printf(TEXT("MoveSpeed %d"),mMoveSpeed));
		}
	}
}



