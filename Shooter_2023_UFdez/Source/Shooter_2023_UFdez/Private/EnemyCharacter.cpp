
#include "EnemyCharacter.h"

#include "Components/BoxComponent.h"
#include "Shooter_2023_UFdez/Public/Utils.h"

AEnemyCharacter::AEnemyCharacter()
{
	PrimaryActorTick.bCanEverTick = true;

	mpBoxCollision=CreateDefaultSubobject<UBoxComponent>(TEXT("BoxCollider"));
	mpBoxCollision->SetupAttachment(RootComponent);

	OnTakeAnyDamage.AddDynamic(this,&AEnemyCharacter::OnHitCallback);

	health=20;
}

void AEnemyCharacter::BeginPlay()
{
	Super::BeginPlay();
	
}

void AEnemyCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AEnemyCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

}

void AEnemyCharacter::OnHitCallback(AActor* apDamagedActor, float apDamage, const UDamageType* apDamageType,
	AController* apInstigatedBy, AActor* apDamageCauser)
{
	health-=apDamage;
	if(health<=0)
	{
		AEnemyCharacter::Destroy();
	}
}

