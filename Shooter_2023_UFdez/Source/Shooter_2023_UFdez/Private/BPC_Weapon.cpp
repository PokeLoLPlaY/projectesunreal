
#include "BPC_Weapon.h"
#include "Utils.h"
#include "PlayerCharacter.h"
#include "Commandlets/GatherTextCommandlet.h"
#include "Components/BillboardComponent.h"
#include "Kismet/GameplayStatics.h"
#include "Kismet/KismetMathLibrary.h"
#include "Shooter_2023_UFdez/Public/Utils.h"

UBPC_Weapon::UBPC_Weapon()
{
	PrimaryComponentTick.bCanEverTick = true;

	mpMuzzelOffset= CreateDefaultSubobject<UBillboardComponent>(TEXT("ShootPoint"));
}

void UBPC_Weapon::AttachWeapoon(APlayerCharacter* apPlayerCharacter)
{
	
	mpOwnerCharacter=apPlayerCharacter;
	
	if(mpOwnerCharacter!=nullptr && mpOwnerCharacter->mpWeaponEquiped==nullptr)
	{
		
		mpOwnerCharacter->mpWeaponEquiped=this;
		FAttachmentTransformRules AttachRules{EAttachmentRule::SnapToTarget,true};
		GetOwner()->AttachToComponent(mpOwnerCharacter->GetWeapon(),AttachRules,TEXT("WeaponPointSocket"));

		mpOwnerCharacter->evOnClick.AddDynamic(this,&UBPC_Weapon::OnFireCallback);
	}
}

void UBPC_Weapon::OnFireCallback()
{
	//ScreenD(TEXT("PIU PIU"))

	if(mpOwnerCharacter==nullptr) return;

	const UWorld* pWorld{GetWorld()};
	if(pWorld==nullptr) return;

	const UCameraComponent* PlayerCamera{mpOwnerCharacter->GetCamera()};

	//Desde el frente de la camara
	//const FVector StartLocation {PlayerCamera->GetComponentLocation()};
	
	const FRotator CameraRotation {PlayerCamera->GetComponentRotation()};
	const FVector StartLocation {GetOwner()->GetActorLocation() + CameraRotation.RotateVector(mpMuzzelOffset->GetComponentLocation())};
	
	FCollisionQueryParams QueryParams {};
	QueryParams.AddIgnoredActor(mpOwnerCharacter);
	for(int i=0;i<mNumBalas;i++)
	{
		const FVector EndLocation {StartLocation + UKismetMathLibrary::GetForwardVector(PlayerCamera->GetComponentRotation())*mShootRange+FVector(FMath::RandRange(-mDispersion, mDispersion), FMath::RandRange(-mDispersion, mDispersion), FMath::RandRange(-mDispersion, mDispersion))};

		FHitResult HitResult {};
		pWorld->LineTraceSingleByChannel(HitResult,StartLocation,EndLocation,ECC_Camera,QueryParams);
		DrawDebugLine(pWorld,StartLocation, EndLocation, HitResult.bBlockingHit ? FColor::Blue: FColor::Red,false, 5.0f,0,10.f);

		if(HitResult.bBlockingHit && IsValid(HitResult.GetActor()))
		{
			UGameplayStatics::ApplyDamage(HitResult.GetActor(), mDamage,mpOwnerCharacter->GetController(),GetOwner(),{});
		}
	}
}

void UBPC_Weapon::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

}

