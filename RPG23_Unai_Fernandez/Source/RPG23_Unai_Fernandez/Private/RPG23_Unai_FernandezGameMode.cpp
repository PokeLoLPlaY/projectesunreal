// Copyright Epic Games, Inc. All Rights Reserved.

#include "RPG23_Unai_FernandezGameMode.h"
#include "RPG23_Unai_FernandezPlayerController.h"
#include "RPG23_Unai_FernandezCharacter.h"
#include "UObject/ConstructorHelpers.h"

ARPG23_Unai_FernandezGameMode::ARPG23_Unai_FernandezGameMode()
{
	// use our custom PlayerController class
	PlayerControllerClass = ARPG23_Unai_FernandezPlayerController::StaticClass();

	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/TopDown/Blueprints/BP_TopDownCharacter"));
	if (PlayerPawnBPClass.Class != nullptr)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}

	// set default controller to our Blueprinted controller
	static ConstructorHelpers::FClassFinder<APlayerController> PlayerControllerBPClass(TEXT("/Game/TopDown/Blueprints/BP_TopDownPlayerController"));
	if(PlayerControllerBPClass.Class != NULL)
	{
		PlayerControllerClass = PlayerControllerBPClass.Class;
	}
}