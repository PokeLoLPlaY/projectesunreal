// Copyright Epic Games, Inc. All Rights Reserved.

#include "RPG23_Unai_FernandezPlayerController.h"
#include "GameFramework/Pawn.h"
#include "Blueprint/AIBlueprintHelperLibrary.h"
#include "NiagaraSystem.h"
#include "NiagaraFunctionLibrary.h"
#include "RPG23_Unai_FernandezCharacter.h"
#include "Engine/World.h"
#include "EnhancedInputComponent.h"
#include "EnhancedInputSubsystems.h"
#include "InputConfigData.h"
#include "RPG23_Unai_Fernandez/Public/Utils.h"

ARPG23_Unai_FernandezPlayerController::ARPG23_Unai_FernandezPlayerController()
{
	bShowMouseCursor = true;
	DefaultMouseCursor = EMouseCursor::Default;
	CachedDestination = FVector::ZeroVector;
	FollowTime = 0.f;
}




void ARPG23_Unai_FernandezPlayerController::BeginPlay()
{
	// Call the base class  
	Super::BeginPlay();
	mpPlayerCharacter = Cast<ARPG23_Unai_FernandezCharacter>(GetCharacter());
	//Add Input Mapping Context
	if (UEnhancedInputLocalPlayerSubsystem* Subsystem = ULocalPlayer::GetSubsystem<UEnhancedInputLocalPlayerSubsystem>(GetLocalPlayer()))
	{
		Subsystem->AddMappingContext(DefaultMappingContext, 0);
	}

	
}

void ARPG23_Unai_FernandezPlayerController::SetupInputComponent()
{
	// set up gameplay key bindings
	Super::SetupInputComponent();

	UEnhancedInputComponent* EnhancedInputComponent {Cast<UEnhancedInputComponent>(InputComponent)};
	
	// Setup mouse input events
	EnhancedInputComponent->BindAction(mInputData->InputLeftClick, ETriggerEvent::Started, this, &ARPG23_Unai_FernandezPlayerController::OnInputStarted);
	EnhancedInputComponent->BindAction(mInputData->InputLeftClick, ETriggerEvent::Triggered, this, &ARPG23_Unai_FernandezPlayerController::OnSetDestinationTriggered);
	EnhancedInputComponent->BindAction(mInputData->InputLeftClick, ETriggerEvent::Completed, this, &ARPG23_Unai_FernandezPlayerController::OnSetDestinationReleased);
	EnhancedInputComponent->BindAction(mInputData->InputLeftClick, ETriggerEvent::Canceled, this, &ARPG23_Unai_FernandezPlayerController::OnSetDestinationReleased);
	EnhancedInputComponent->BindAction(mInputData->InputRightClick, ETriggerEvent::Completed, this, &ARPG23_Unai_FernandezPlayerController::OnActPressed);

	
	for (int ButtonIndex{0}; ButtonIndex< mInputData->mpInputSkills.Num();++ButtonIndex)
	{
		EnhancedInputComponent->BindAction(mInputData->mpInputSkills[ButtonIndex],ETriggerEvent::Completed,this,&ARPG23_Unai_FernandezPlayerController::OnSkillPressed,ButtonIndex);
	}
}

void ARPG23_Unai_FernandezPlayerController::OnInputStarted()
{
	StopMovement();
}

// Triggered every frame when the input is held down
void ARPG23_Unai_FernandezPlayerController::OnSetDestinationTriggered()
{
	// We flag that the input is being pressed
	FollowTime += GetWorld()->GetDeltaSeconds();
	
	// We look for the location in the world where the player has pressed the input
	FHitResult Hit;
	bool bHitSuccessful = false;
	if (bIsTouch)
	{
		bHitSuccessful = GetHitResultUnderFinger(ETouchIndex::Touch1, ECollisionChannel::ECC_Visibility, true, Hit);
	}
	else
	{
		bHitSuccessful = GetHitResultUnderCursor(ECollisionChannel::ECC_Visibility, true, Hit);
	}

	// If we hit a surface, cache the location
	if (bHitSuccessful)
	{
		CachedDestination = Hit.Location;
	}
	
	// Move towards mouse pointer or touch
	APawn* ControlledPawn = GetPawn();
	if (ControlledPawn != nullptr)
	{
		FVector WorldDirection = (CachedDestination - ControlledPawn->GetActorLocation()).GetSafeNormal();
		ControlledPawn->AddMovementInput(WorldDirection, 1.0, false);
	}
}

void ARPG23_Unai_FernandezPlayerController::OnSetDestinationReleased()
{
	// If it was a short press
	if (FollowTime <= ShortPressThreshold)
	{
		// We move there and spawn some particles
		UAIBlueprintHelperLibrary::SimpleMoveToLocation(this, CachedDestination);
		UNiagaraFunctionLibrary::SpawnSystemAtLocation(this, FXCursor, CachedDestination, FRotator::ZeroRotator, FVector(1.f, 1.f, 1.f), true, true, ENCPoolMethod::None, true);
		
	}

	FollowTime = 0.f;
}

// Triggered every frame when the input is held down
void ARPG23_Unai_FernandezPlayerController::OnTouchTriggered()
{
	bIsTouch = true;
	OnSetDestinationTriggered();
}

void ARPG23_Unai_FernandezPlayerController::OnTouchReleased()
{
	bIsTouch = false;
	OnSetDestinationReleased();
}

void ARPG23_Unai_FernandezPlayerController::OnSkillPressed(int aButtonPressed)
{
	
	if(auto SkillPressed{mSkills[aButtonPressed]};SkillPressed != ESkill::NONE)
	{
		
		if(auto* Skill{GetSkill(SkillPressed)}; Skill)
		{
		
			FString s= *Skill->Description;
			GEngine->AddOnScreenDebugMessage(-1, 2.0f, FColor::Yellow, s);
			mSkillSelected= *Skill;
		}
	}
	
}

void ARPG23_Unai_FernandezPlayerController::OnActPressed(const FInputActionValue& aValue)
{
			
	if(mSkillSelected.Name==ESkill::NONE) return;
	
	FVector HitLocation {FVector::ZeroVector};
	GetHitResultUnderCursorByChannel(TraceTypeQuery1,true, mHitResult);

	evOnLocationClick.Broadcast(mHitResult.Location, mSkillSelected);
}

FSkillDataRow* ARPG23_Unai_FernandezPlayerController::GetSkill(ESkill aSkill)
{

	FSkillDataRow* SkillFound{};
	if(mSkillDB)
	{
		
		FName SkillString{UEnum::GetDisplayValueAsText(aSkill).ToString()};
		static FString FindContext {FString("Searching for ").Append(SkillString.ToString())};
		SkillFound = mSkillDB->FindRow<FSkillDataRow>(SkillString, FindContext, true);
	}
	return SkillFound;
}
