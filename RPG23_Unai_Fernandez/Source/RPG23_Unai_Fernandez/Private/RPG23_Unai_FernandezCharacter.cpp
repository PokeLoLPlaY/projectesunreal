// Copyright Epic Games, Inc. All Rights Reserved.

#include "RPG23_Unai_FernandezCharacter.h"

#include "ClassDataRow.h"
#include "UObject/ConstructorHelpers.h"
#include "Camera/CameraComponent.h"
#include "Components/DecalComponent.h"
#include "Components/CapsuleComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "GameFramework/PlayerController.h"
#include "GameFramework/SpringArmComponent.h"
#include "Materials/Material.h"
#include "Engine/World.h"

ARPG23_Unai_FernandezCharacter::ARPG23_Unai_FernandezCharacter()
{
	// Set size for player capsule
	GetCapsuleComponent()->InitCapsuleSize(42.f, 96.0f);

	// Don't rotate character to camera direction
	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = false;
	bUseControllerRotationRoll = false;

	// Configure character movement
	GetCharacterMovement()->bOrientRotationToMovement = true; // Rotate character to moving direction
	GetCharacterMovement()->RotationRate = FRotator(0.f, 640.f, 0.f);
	GetCharacterMovement()->bConstrainToPlane = true;
	GetCharacterMovement()->bSnapToPlaneAtStart = true;

	// Create a camera boom...
	CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
	CameraBoom->SetupAttachment(RootComponent);
	CameraBoom->SetUsingAbsoluteRotation(true); // Don't want arm to rotate when character does
	CameraBoom->TargetArmLength = 800.f;
	CameraBoom->SetRelativeRotation(FRotator(-60.f, 0.f, 0.f));
	CameraBoom->bDoCollisionTest = false; // Don't want to pull camera in when it collides with level

	// Create a camera...
	TopDownCameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("TopDownCamera"));
	TopDownCameraComponent->SetupAttachment(CameraBoom, USpringArmComponent::SocketName);
	TopDownCameraComponent->bUsePawnControlRotation = false; // Camera does not rotate relative to arm

	// Activate ticking in order to update the cursor every frame.
	PrimaryActorTick.bCanEverTick = true;
	PrimaryActorTick.bStartWithTickEnabled = true;
}

void ARPG23_Unai_FernandezCharacter::Tick(float DeltaSeconds)
{
    Super::Tick(DeltaSeconds);
}

void ARPG23_Unai_FernandezCharacter::BeginPlay()
{
	Super::BeginPlay();



	
	FClassDataRow* classDataRow = GetClass(Class);

	if(classDataRow)
	{
		GEngine->AddOnScreenDebugMessage(-1, 2.0f, FColor::Yellow, classDataRow->Description);
		
		HPmax = classDataRow->HP;
		HP=HPmax;
		Dmg = classDataRow->BaseDamage;
		Spd = classDataRow->Speed;
		Def = classDataRow->Defensa;
		
	}
	
	
}
FClassDataRow* ARPG23_Unai_FernandezCharacter::GetClass(EClass aClass)
{
	FClassDataRow* ClassFound{};

	if(mClassDB)
	{
		FName ClassString{UEnum::GetDisplayValueAsText(aClass).ToString()};
		static FString FindContext{FString("Searching for ").Append(ClassString.ToString())};
		ClassFound = mClassDB->FindRow<FClassDataRow>(ClassString, FindContext, true);
	}

	return ClassFound;
}