// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "SkillBase.generated.h"

UCLASS()
class RPG23_UNAI_FERNANDEZ_API ASkillBase : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ASkillBase();

	virtual void Tick(float DeltaTime) override;
	UPROPERTY(BlueprintReadWrite,EditAnywhere, meta=(ExposeOnSpawn))
	AActor* mptarget;

	UPROPERTY(BlueprintReadWrite,EditAnywhere, meta=(ExposeOnSpawn))
		float aDamage;

	UPROPERTY(BlueprintReadWrite,EditAnywhere, meta=(ExposeOnSpawn))
		float mDuration;
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
};
