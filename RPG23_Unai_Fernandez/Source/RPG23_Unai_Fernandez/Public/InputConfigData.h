#pragma once

#include "CoreMinimal.h"
#include "Engine/DataAsset.h"
#include "InputConfigData.generated.h"

class UInputAction;

UCLASS()
class RPG23_UNAI_FERNANDEZ_API UInputConfigData : public UDataAsset
{
	GENERATED_BODY()
public:
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
		UInputAction* InputLeftClick;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
		UInputAction* InputRightClick;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
		TArray<UInputAction*> mpInputSkills;
};
