
#pragma once

#include "CoreMinimal.h"
#include "Engine/DataTable.h"
#include "Engine/Texture2D.h"
#include "ClassDataRow.generated.h"



UENUM(BlueprintType)
enum class EClass 
{
	NONE = 0 UMETA(DisplayName="None"),
	GUERRERO UMETA(DisplayName="Guerrero"),
	MAGO UMETA(DisplayName="Mago"),
	ARQUERO UMETA(DisplayName="Arquero"),
};

USTRUCT(Blueprintable,BlueprintType)
struct RPG23_UNAI_FERNANDEZ_API FClassDataRow : public FTableRowBase
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		EClass Class{EClass::NONE};

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		FString Description;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		float HP;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		float BaseDamage;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		float Speed;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		float Defensa;
};
