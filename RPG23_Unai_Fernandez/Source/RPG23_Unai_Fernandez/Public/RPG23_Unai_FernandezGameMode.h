// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "RPG23_Unai_FernandezGameMode.generated.h"

UCLASS(minimalapi)
class ARPG23_Unai_FernandezGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	ARPG23_Unai_FernandezGameMode();
};



