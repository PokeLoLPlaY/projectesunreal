// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "Templates/SubclassOf.h"
#include "GameFramework/PlayerController.h"
#include "InputActionValue.h"
#include "RPG23_Unai_FernandezCharacter.h"
#include "SkillDataRow.h"
#include "RPG23_Unai_FernandezPlayerController.generated.h"

/** Forward declaration to improve compiling times */
class UNiagaraSystem;
class UInputConfigData;

DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnLocationCLick, FVector, aClickLocation, const FSkillDataRow&,
                                             aSkillData);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnDirectionSkillCast, FRotator, aRotation,FSkillDataRow, aSkillData);
UCLASS()

class ARPG23_Unai_FernandezPlayerController : public APlayerController
{
	GENERATED_BODY()

public:
	ARPG23_Unai_FernandezPlayerController();

	/** Time Threshold to know if it was a short press */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Input)
	float ShortPressThreshold;

	/** FX Class that we will spawn when clicking */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Input)
	UNiagaraSystem* FXCursor;

	/** MappingContext */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category=Input, meta=(AllowPrivateAccess = "true"))
	class UInputMappingContext* DefaultMappingContext;


	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
	UInputConfigData* mInputData;

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	ARPG23_Unai_FernandezCharacter* mpPlayerCharacter;


	
#pragma region SKILLS

	FSkillDataRow* GetSkill(ESkill skill);
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category=mSKILL)
	TArray<ESkill> mSkills;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category=mSKILL)
	UDataTable* mSkillDB;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category=mSKILL)
	FSkillDataRow mSkillSelected;

#pragma endregion

#pragma region EVENTS

	UPROPERTY(BlueprintAssignable, BlueprintCallable)
		FOnLocationCLick evOnLocationClick;

	UPROPERTY(BlueprintAssignable,BlueprintCallable)
		FOnDirectionSkillCast evOnDirectionSkillCast;
#pragma endregion

protected:
	/** True if the controlled character should navigate to the mouse cursor. */
	uint32 bMoveToMouseCursor : 1;

	virtual void SetupInputComponent() override;

	// To add mapping context
	virtual void BeginPlay();

	/** Input handlers for SetDestination action. */
	void OnInputStarted();
	void OnSetDestinationTriggered();
	void OnSetDestinationReleased();
	void OnTouchTriggered();
	void OnTouchReleased();

	void OnSkillPressed(int aButtonPressed);
	void OnActPressed(const FInputActionValue& aValue);

private:
	FVector CachedDestination;
	FHitResult mHitResult{};

	bool bIsTouch; // Is it a touch device
	float FollowTime; // For how long it has been pressed
};
