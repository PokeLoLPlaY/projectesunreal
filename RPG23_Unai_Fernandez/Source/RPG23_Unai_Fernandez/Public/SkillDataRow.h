
#pragma once

#include "CoreMinimal.h"
#include "Engine/DataTable.h"
#include "Engine/Texture2D.h"
#include "SkillDataRow.generated.h"


class UTexture2D;
class ASkillBase;

UENUM(BlueprintType)
enum class ESkill 
{
	NONE = 0 UMETA(DisplayName="None"),
	FIREBALL UMETA(DisplayName="Fireball")
};

USTRUCT(Blueprintable,BlueprintType)
struct RPG23_UNAI_FERNANDEZ_API FSkillDataRow : public FTableRowBase
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		ESkill Name{ESkill::NONE};

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		FString Description;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		TWeakObjectPtr<UTexture2D> Image {nullptr};

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		TSubclassOf<ASkillBase> Skill{nullptr};

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		float BaseDamage;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		FVector Scale {1.f};
};
