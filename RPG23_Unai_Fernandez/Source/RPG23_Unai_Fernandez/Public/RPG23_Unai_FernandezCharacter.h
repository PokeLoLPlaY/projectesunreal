// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "ClassDataRow.h"
#include "RPG23_Unai_FernandezCharacter.generated.h"

UCLASS(Blueprintable)
class ARPG23_Unai_FernandezCharacter : public ACharacter
{
	GENERATED_BODY()
protected:
	virtual void BeginPlay();
	
public:
	ARPG23_Unai_FernandezCharacter();

	// Called every frame.
	virtual void Tick(float DeltaSeconds) override;

	/** Returns TopDownCameraComponent subobject **/
	FORCEINLINE class UCameraComponent* GetTopDownCameraComponent() const { return TopDownCameraComponent; }
	/** Returns CameraBoom subobject **/
	FORCEINLINE class USpringArmComponent* GetCameraBoom() const { return CameraBoom; }

#pragma region Class

	FClassDataRow* GetClass(EClass Class);
	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	UDataTable* mClassDB;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category=mCLASS)
	TArray<EClass> mClass;
	
	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	float HPmax;

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	EClass Class;
	
	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	float HP;

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	float Dmg;
	
	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	float Def;

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	float Spd;

#pragma endregion
private:
	/** Top down camera */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class UCameraComponent* TopDownCameraComponent;

	/** Camera boom positioning the camera above the character */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class USpringArmComponent* CameraBoom;
};

