// Copyright Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;

public class RPG23_Unai_Fernandez : ModuleRules
{
	public RPG23_Unai_Fernandez(ReadOnlyTargetRules Target) : base(Target)
	{
		PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;

        PublicDependencyModuleNames.AddRange(new string[] { "Core", "CoreUObject", "Engine", "InputCore", "HeadMountedDisplay", "NavigationSystem", "AIModule", "Niagara", "EnhancedInput" });
    }
}
