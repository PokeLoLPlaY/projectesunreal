// Copyright Epic Games, Inc. All Rights Reserved.

#include "RPG23_Unai_Fernandez.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, RPG23_Unai_Fernandez, "RPG23_Unai_Fernandez" );

DEFINE_LOG_CATEGORY(LogRPG23_Unai_Fernandez)
 